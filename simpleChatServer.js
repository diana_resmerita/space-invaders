
//-----------------------------------------------------------------------------
//					/*\ PARAMETRAGE NE PAS TOUCHER /*\
//-----------------------------------------------------------------------------
// We need to use the express framework: have a real web servler 
//that knows how to send mime types etc.
var express=require('express');

// Init globals variables for each module required
var app = express()
  , http = require('http')
  , server = http.createServer(app)
  , io = require('socket.io').listen(server);

// launch the http server on given port
server.listen(8082);

// Indicate where static files are located. Without this, no external js file, no css...  
app.use(express.static(__dirname + '/'));    


// routing
app.get('/', function (req, res) {
  res.sendfile(__dirname + '/Accueil.html');
});

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------



// usernames which are currently connected to the chat
var usernames = {};
usernames['Accueil'] = {};
var listOfPlayers = {};
var compt = 0;
var games = {};
games['Accueil'] = {};
var CompteurConnexion = 0;
var usersPatron = {};

//-----------------------------------------------------------------------------
//									PART OF SOCKET
//-----------------------------------------------------------------------------
io.sockets.on('connection', function (socket) {

	var socketId = socket.id;
	var clientIp = socket.request.connection.remoteAddress;

	console.log(clientIp);
	
	/*	if(clientIp.split(":")[3].indexOf("127.0.0") != -1){
		// ceci est le client-serveur
		io.sockets.emit('imserver', socket.username, );
	}*/
	
	//-------------------------------------------------------------------------
	// 								sendchat
	//-------------------------------------------------------------------------
	socket.on('sendchat', function (data) {
			
		r = listOfPlayers[socket.room][socket.username].colorTexte["r"];
		g = listOfPlayers[socket.room][socket.username].colorTexte["g"];
		b = listOfPlayers[socket.room][socket.username].colorTexte["b"];
		
		color = "rgba('"+r+","+g+","+b+"')";	
		
		io.sockets.emit('updatechat', socket.username, "<font color='"+color+"'>" + data +"</font>");
			
	});
	//-------------------------------------------------------------------------
	//------------------------------------------------------------------------- 
	
	//-------------------------------------------------------------------------
	// 								valide
	//-------------------------------------------------------------------------
	socket.on('valide', function (pseudo,Namegame) {
		
		if(Namegame in games){
			if(pseudo in games[Namegame]){
				socket.emit('invalide');
			}else if(pseudo == '' || pseudo == undefined) {
				socket.emit('invalide');
			}else{
				// ici le pseudo et valide 
				socket.emit('valide');
			}
		}else{
			socket.emit('valide');
		}
			
	});
	//-------------------------------------------------------------------------
	//------------------------------------------------------------------------- 
	
	//-------------------------------------------------------------------------
	// 								updateCompt
	//-------------------------------------------------------------------------
	socket.on('updateCompt', function (Namegame,compt) {
			
		socket.broadcast.to(socket.room).emit('updateCompt', compt);
			
	});
	//-------------------------------------------------------------------------
	//------------------------------------------------------------------------- 
	
	//-------------------------------------------------------------------------
	// 								ChangeAlienVisuel
	//-------------------------------------------------------------------------
	socket.on('ChangeAlienVisuel', function (Namegame) {
			
		io.in(socket.room).emit('ChangeAlienVisuel');
			
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------

	//-------------------------------------------------------------------------
	// 								updateusers
	//-------------------------------------------------------------------------
	socket.on('updateusers', function () {
	
		console.log("updtaeuser");	
		io.in(socket.room).emit('updateusers', usernames[socket.room]);	
			
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								tirJoueur
	//-------------------------------------------------------------------------
	socket.on('tirJoueur', function (username , vitesse , game,x,y) {

		//console.log("recu TIR Joueur");

		try {

			io.in(socket.room).emit('tirJoueur', username , vitesse, x,y );

		}catch (e) {
		   console.log("ERREUR :  " + e);
		}
		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								tirAlien
	//-------------------------------------------------------------------------
	socket.on('tirAlien', function (vitesse , game,x,y, lig , col ) {

		//console.log("recu TIR Alien");

		try {

			io.in(socket.room).emit('tirAlien', vitesse, x,y,lig,col );

		}catch (e) {
		   console.log("ERREUR :  " + e);
		}
		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								moveAlien
	//-------------------------------------------------------------------------
	socket.on('moveAlien', function (x ,y ,lig ,col,NameGame ,compt) {

		//console.log("recu moveAlien" );

			try {
			
				//socket.broadcast.to(NameGame).emit('moveAlien', x ,y ,lig ,col,compt );
				io.in(socket.room).emit('moveAlien', x ,y ,lig ,col,compt );
				

			}catch (e) {
			   console.log("ERREUR :  " + e);
			}		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								moveVaisseauRouge
	//-------------------------------------------------------------------------
	socket.on('moveVaisseauRouge', function (x ,y ) {

		//console.log("recu moveAlien" );

			try {
			
				io.in(socket.room).emit('moveVaisseauRouge', x ,y );
				

			}catch (e) {
			   console.log("ERREUR :  " + e);
			}		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								moveMissile
	//-------------------------------------------------------------------------
	socket.on('moveMissile', function (id ,y ) {

		//console.log("recu moveMissile" );

			try {
			
				io.in(socket.room).emit('moveMissile',id, y );
				

			}catch (e) {
			   console.log("ERREUR :  " + e);
			}		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								updatepos
	//-------------------------------------------------------------------------
	socket.on('updatepos', function (username ,x ,y ,NameGame) {

		//console.log("recu move joueur" );

			try {
			
				io.in(socket.room).emit('updatepos', username , x ,y  );

			}catch (e) {
			   console.log("ERREUR :  " + e);
			}		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								updatebouclier
	//-------------------------------------------------------------------------
	socket.on('updatebouclier', function (tab) {

		//console.log("recu updatebouclier" );

			try {
			
				socket.broadcast.to(socket.room).emit('updatebouclier', tab );

			}catch (e) {
			   console.log("ERREUR :  " + e);
			}		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								updatelevel
	//-------------------------------------------------------------------------
	socket.on('updatelevel', function (niveau) {

		//console.log("recu updatelevel" );

			try {
			
				socket.broadcast.to(socket.room).emit('updatelevel', niveau );

			}catch (e) {
			   console.log("ERREUR :  " + e);
			}		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								updateAlien
	//-------------------------------------------------------------------------
	socket.on('updateAlien', function (tab) {

		//console.log("recu updateAlien" );

			try {
			
				socket.broadcast.to(socket.room).emit('updateAlien', tab );

			}catch (e) {
			   console.log("ERREUR :  " + e);
			}		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								GAME_OVER
	//-------------------------------------------------------------------------
	socket.on('GAME_OVER', function () {

		console.log("GAME_OVER" );

			try {
				
				listOfPlayers[socket.room][socket.username].game_over = true;
				io.emit('updateListGames',games,listOfPlayers);
				io.in(socket.room).emit('GAME_OVER', socket.username );
				usersPatron[socket.room] = usersPatron[socket.room]-1;
				console.log("Patron : " + listOfPlayers[socket.room][socket.username].patron);
				console.log(listOfPlayers[socket.room][socket.username]);
				
				if(listOfPlayers[socket.room][socket.username].patron){
					//elir un nouveau patron
					for(name in games[socket.room]){
						if(name != 0 && name != socket.username ){
							console.log("Patron trouve" + name);
							socket.broadcast.to(games[socket.room][name]).emit('ChoosePatron');
							listOfPlayers[socket.room][name].patron = true;
							break;
						}
					}
				}

			}catch (e) {
			   console.log("ERREUR :  " + e);
			}		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	// 								deco
	//-------------------------------------------------------------------------
	socket.on('disconnect', function () {
		console.log("start Deconnexion----------------------------------------------------");
		patron = false;
		user = socket.username;
		if(user != undefined && socket != undefined){
			
			delete usernames[socket.room][user];
			console.log("user  " + user);
			
			//io.in(socket.room).emit('updateusers', usernames[socket.room]);
			
			if(socket.room != 'Accueil' && socket.room != ''){
				
				console.log("socket.room  " + socket.room);
				patron = listOfPlayers[socket.room][user].patron;
				delete listOfPlayers[socket.room][user];		
				io.in(socket.room).emit('updatePlayers',listOfPlayers[socket.room]);
				
				io.in(socket.room).emit('decoPlayer',user);
			
				// echo globally that this client has left
				socket.broadcast.emit('updatechat', "<font color='red' >" + 'SERVER' + "</font>", user + ' has disconnecteds');
				
			}
			
			longueur = Object.keys(games[socket.room]).length
			for(index in games[socket.room]){

				if(index == user){	
								
					if(longueur == 2 && socket.room != 'Accueil'){
						console.log("DELETE GAME");
						delete games[socket.room];
						delete usersPatron[socket.room];
					}else{
						delete games[socket.room][index];
						if(socket.room != 'Accueil'){
							usersPatron[socket.room] = usersPatron[socket.room]-1;
							console.log("Patron : " + patron);
							if(patron){
								//elir un nouveau patron
								for(name in games[socket.room]){
									if(name != 0){
										socket.broadcast.to(games[socket.room][name]).emit('ChoosePatron');
										listOfPlayers[socket.room][name].patron = true;
										break;
									}
								}
							}
						}
					}
				}	
			}
			
			io.emit('updateListGames',games,listOfPlayers);
			
			socket.leave(socket.room);
			console.log(listOfPlayers);
			console.log(games);
			console.log("DECONNEXION  " + user );
			console.log("End Deconnexion----------------------------------------------------");
		}
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//								CO_Client
	//-------------------------------------------------------------------------
	socket.on('coclient', function(username){
	
		CompteurConnexion+=1;
		
		if (username == 'essaie'){
			username = username + CompteurConnexion;
			socket.emit('updateName', username);
		}
		
		socket.username = username;
		
		socket.room = 'Accueil';
		
		usernames[socket.room][username] = username;
		
		socket.join('Accueil');

		games['Accueil'][username] = username;
		
		io.emit('updateListGames',games,listOfPlayers);
		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	//					Passage d un joueur dans une game
	//-------------------------------------------------------------------------
	socket.on('createPart', function(oldPseudo, pseudo, NameGame, NbPlayers,w,h){
		
		socket.leave(socket.room);
		
		socket.username = pseudo;
		
		delete usernames['Accueil'][oldPseudo];
		usernames[NameGame] = {};
		usernames[NameGame][pseudo] = pseudo;
		
		socket.join(NameGame);
		
		socket.room = NameGame;
		
		tabInfoGame = {};
		tabInfoGame[0] = NbPlayers;
		tabInfoGame[pseudo] = socket.id;
		
		games[NameGame] = tabInfoGame;
		
		io.emit('updateListGames',games,listOfPlayers);
		
		color = generateColor();
		
		var player = {'colorTexte' : color , 'patron' : false, 'Game_Over' : false};
		
		listOfPlayers[NameGame] = {};
		
		listOfPlayers[NameGame][pseudo] = player;
		
		console.log(listOfPlayers[NameGame]);
		
		io.in(NameGame).emit('updatePlayers',listOfPlayers[NameGame]);
		
		io.in(NameGame).emit('updateusers', usernames[NameGame]);

		socket.emit('MiseEnplaceGame', NameGame);
		
		
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	// 								GotoGame
	//-------------------------------------------------------------------------
	socket.on('GotoGame', function (oldPseudo, pseudo, NameGame) {

		socket.leave(socket.room);
		
		socket.username = pseudo;
		
		delete usernames['Accueil'][oldPseudo];
		usernames[NameGame][pseudo] = pseudo;
		
		socket.join(NameGame);
		
		socket.room = NameGame;
				
		games[NameGame][pseudo] = socket.id;
		
		io.emit('updateListGames',games,listOfPlayers);
		
		color = generateColor();
		
		var player = {'colorTexte' : color , 'patron' : false , 'Game_Over' : false};
		
		listOfPlayers[NameGame][pseudo] = player;
		
		console.log(listOfPlayers[NameGame]);
		
		io.in(NameGame).emit('updatePlayers',listOfPlayers[NameGame]);
		
		socket.emit('MiseEnplaceGame', NameGame);
		
		if(games[NameGame][0] == (Object.keys(games[socket.room]).length -1)){
			io.in(NameGame).emit('Lancement');
		}

	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
		
	//-------------------------------------------------------------------------
	//								demarrage
	//-------------------------------------------------------------------------
	socket.on('demarrage', function (username, game) {

		console.log("recu demarrage" );

		try {
		
			if(game in usersPatron){
				usersPatron[game] = usersPatron[game] + 1 ;
			}else{
				usersPatron[game] = 1;
			}
			
			if(usersPatron[game] == games[game][0]){
				existAlready = false;
				for(name in listOfPlayers[game]){
					if(listOfPlayers[game][name].patron){
						existAlready = true;
					}
				}
				if(!existAlready){
					listOfPlayers[game][username].patron = true;
					socket.emit('ChoosePatron');
				}
			}
			console.log("PATRON : " );
			console.log(listOfPlayers);
			console.log(usersPatron);
		}catch (e) {
		   console.log("ERREUR :  " + e);
		}		
			
	});
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
});


//-----------------------------------------------------------------------------
//									UTILS
//-----------------------------------------------------------------------------

function generateColor(){

	r = ( Math.random() * 255) + 100 ;
	g = ( Math.random() * 255) ;
	b = ( Math.random() * 255) + 100 ;

	console.log("rgba("+r+","+g+","+b+",1.0)");
	
	tab = {};
	tab["r"] = r;
	tab["g"] = g;
	tab["b"] = b;
	
return tab;

}