
function Missile(xd,yd,tX, tY ,tXC, tYC, Cx , Cy , Cr,Cg,Cb ,identification){
	
	var id = identification;
	var x = xd;
	var y = yd;
	var speedY = 0;
	
	var tXCrop = tXC;
	var tYCrop = tYC;
	var cropX = Cx;
	var cropY = Cy;
	var tailleX = tX;
	var tailleY = tY;
	
	var r = Cr;
	var g = Cg;
	var b = Cb;
	
	var getSpeedY = function(value){
		return speedY;
	}
	var setSpeedY = function(value){
		speedY = value;
	}
	
	var effacer = function(){
		ctx.clearRect(x, y,tailleX,tailleY);  
	};

	var draw = function(ctx){
		return drawMissile(ctx);
	};

	var drawMissile = function(ctx){
	  
		// changement Image
		if(compt%5 == 0){	
			changeImageMissile = !changeImageMissile; 
		}
		
		if(changeImageMissile){
			cropY = 607;
		}else{
			cropY = 627.5;
		}
		
		if(testCollision()){
			
			effacer();
			move();	
			ctx.drawImage(spritesheet2, 
					cropX, cropY, // debut de ou commencer le crop
					tXCrop, tYCrop, // taille image
					x, y, // coordonné sur le dessin
					tailleX, tailleY); // taiile image
					
			return true;
		}else{
			//console.log("EFFACER");
			effacer();
			return false;
		}
	/*	imageData = ctx.getImageData(x, y, tailleX, tailleY);
		var data = imageData.data;

		for(var i = 0; i < data.length; i += 4) {
			// red
			data[i] =  data[i] - r;
			// green
			data[i + 1] =  data[i + 1] - g ;
			// blue
			data[i + 2] =   data[i + 2] - b;
		}		

		ctx.putImageData(imageData, x, y);*/
	};
	
	var testCollision = function(){
	  
		value = y + speedY ;

		if(speedY < 0){
			if(value <= 0){
				return false;
			}
		}else{
			if(value >= h){
				return false;
			}
		}
			
		return true;  
		  
	}

	var move = function() {
		//x = x + speedX;
		//y = y + speedY;
		socket.emit('moveMissile',id,y + speedY);
	};
	
	var getY = function(){
		return y;
	}
	
	var setY = function(value){
		y = value;
	}
	
	
	return{
		x:x,
		y:y,
		getY:getY,
		setY:setY,
		width:tailleX,
		height:tailleY,
		setSpeedY:setSpeedY,
		speedY:speedY,
		getSpeedY:getSpeedY,
		move:move,
		testCollision:testCollision,
		draw:draw,
		effacer:effacer
	};
  
  
}
