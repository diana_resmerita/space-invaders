
document.write('<script type="text/javascript" src="Vaisseau.js" ></script>');
document.write('<script type="text/javascript" src="Missile.js"  ></script>');
document.write('<script type="text/javascript" src="Bouclier.js" ></script>');

var canvas, ctx, mousePos , mouseDown , h, w , spritesheet ,spritesheet2,comptWait = 0, compt = 0,normal = true, start_page, gameover_page,
changeImageMissile = true , vitesseDeplacementEnnemi = 20 , niveau = 1;
var chiffres = new Array();
var shootSon, boomSon;

var VaisseauRouge;
var VaisseauEnemis  = new Array();
var VaisseauEnemis2  = new Array();
var missiles = new Array();
var VaisseauJoueur = "";

var Boucliers = new Array();

// Autres joueurs
var OtherPlayersVaisseau = new Array();

// etats du jeu
var etat = {};
etat.JEU_EN_COURS=0;
etat.GAME_OVER = 1;
etat.MENU_DEMARRAGE = 2;
etat.ATTENTE_JOUEURS = 3;
etat.WAIT = 4;

var etatJeu = etat.MENU_DEMARRAGE;

var compteurDemarage = 3;

function init() {

	canvas = document.querySelector("#myCanvas");
	
	w = canvas.width;
	h = canvas.height;

	spritesheet = new Image();
    spritesheet.src = "/images/invaders.gif"; 
	spritesheet2 = new Image();
	spritesheet2.src = "/images/space_invaders_sprite.png";
	for(var i = 0; i < 10; i++){
	chiffres[i] = {'x':(155 + i * 12), 'y': 265, 'width':11, 'height':17};
	}
	
	Canvwidth = 50;
	decalage = 60;
	Canvheight = 40;
	vitesseEnnemis = 10;
	//creationEnemis();
	//creationBoucliers();
	
	shootSon = document.getElementById('shoot-sound');
	boomSon = document.getElementById('boom-sound');
	
	ctx = canvas.getContext('2d');

	// On definit des écouteurs clavier, souris, gamepad 
	document.addEventListener('keydown',move,false);
	document.addEventListener('keyup',arret,false);
	
	mouseDown = false;
	mainloop();

}

function creationEnemis(){
	VaisseauRouge = new Vaisseau(w + 70 ,35,70,Canvheight,125,60,18,634,0,255,255 , "r", [50,100,150,300]);
	
	k = 0;
	for(i = 0; i < 12 ; i++){ 
		VaisseauEnemis[i] = new Array();
		VaisseauEnemis2[i] = new Array();
	}
switch(niveau){
		case 1:
			Enemis_Type1(0);
			break;
		case 2:
			Enemis_Type2(0);
			Enemis_Type1(1);
			break;
		case 3:
			Enemis_Type3(0);
			Enemis_Type2(1);
			Enemis_Type1(2);
			break;
		case 4:
			Enemis_Type3(0);
			Enemis_Type2(1);
			Enemis_Type2(2);
			Enemis_Type1(3);
			Enemis_Type1(4);
			break;
	}

		if (niveau > 4){
			vitesseEnnemis +=10;
			Enemis_Type3(0);
			Enemis_Type2(1);
			Enemis_Type2(2);
			Enemis_Type1(3);
			Enemis_Type1(4);
		}
}

function Enemis_Type1(k){
	for(i = 0; i < 12 ; i++){ 
		VaisseauEnemis[i][k] = new Vaisseau((i+1)*decalage,90 + k * 50,Canvwidth,Canvheight,120,85,20,132,255,0,255, i + " " + k, 10);
		VaisseauEnemis[i][k].setSpeedX(vitesseEnnemis);
		VaisseauEnemis2[i][k] = new Vaisseau((i+1)*decalage,90 + k * 50,Canvwidth,Canvheight,120,85,160,132,255,0,255, i + " " + k, 10);
		VaisseauEnemis2[i][k].setSpeedX(vitesseEnnemis);	
	}
	
}

function Enemis_Type2(k){
	for(i = 0; i < 12 ; i++){ 
		VaisseauEnemis[i][k] = new Vaisseau((i+1)*decalage,90 + k *50,Canvwidth,Canvheight,115,80,15,15,255,255,0, i + " " + k, 20);
		VaisseauEnemis[i][k].setSpeedX(vitesseEnnemis);
		VaisseauEnemis2[i][k] = new Vaisseau((i+1)*decalage,90 + k * 50,Canvwidth,Canvheight,110,80,165,15,255,255,0, i + " " + k, 20);
		VaisseauEnemis2[i][k].setSpeedX(vitesseEnnemis);	
	}
}

function Enemis_Type3(k){
	for(i = 0; i < 12 ; i++){ 
		VaisseauEnemis[i][k] = new Vaisseau((i+1)*decalage,90 + k * 50,Canvwidth,Canvheight,85,80,310,15,0,0,255 , i + " " + k, 30);// width , height ,x ,y
		VaisseauEnemis[i][k].setSpeedX(vitesseEnnemis);
		VaisseauEnemis2[i][k] = new Vaisseau((i+1)*decalage,90 + k * 50,Canvwidth,Canvheight,80,80,430,15,0,0,255, i + " " + k, 30);
		VaisseauEnemis2[i][k].setSpeedX(vitesseEnnemis);
	}
}



function creationJoueur(){

	r = listOfplayers[username].colorTexte["r"];
	g = listOfplayers[username].colorTexte["g"];
	b = listOfplayers[username].colorTexte["b"];
	

	VaisseauJoueur =  new Vaisseau(w / 2 - 125/2,h  - 100,125 , 100 ,86,63, 145,630 , r , g , b , "joueur_" + username);
	
}

function creationBoucliers(){
	for(var i = 0 ; i < 4 ; i++){
		Boucliers[i] = new Bouclier(w/8 + 200 * i, h - 200, 150, 100, 82, 50, 263, 1407);
		Boucliers[i].setImgData(undefined);
	}
}

function creationVaisseauJoueur(nameUtilisateur){

	if(!(nameUtilisateur in OtherPlayersVaisseau)){
		r = listOfplayers[nameUtilisateur].colorTexte["r"];
		g = listOfplayers[nameUtilisateur].colorTexte["g"];
		b = listOfplayers[nameUtilisateur].colorTexte["b"];
		OtherPlayersVaisseau[nameUtilisateur] = new Vaisseau(w / 2 - 125/2,h  - 100,125 , 100 ,86,63, 145,630 , r , g , b , "joueur_" + nameUtilisateur);
	}
}

function updatePlayerNewPos(NameUser, x , y) {

	if(NameUser == username){
		VaisseauJoueur.setX(x);
		VaisseauJoueur.setY(y);
	}else{
		OtherPlayersVaisseau[NameUser].setX(x);
		OtherPlayersVaisseau[NameUser].setY(y);	
	}  
}

function mainloop(time) {
	//on efface le canvas
	ctx.clearRect(0, 0, w, h);
	
	switch(etatJeu) {
    case etat.MENU_DEMARRAGE :
		start_page = new Image();
		start_page.src = "/images/start_page.png";
		//var points_img = new Image();
		//points_img.src = "points.gif"
		ctx.drawImage(start_page, 0, 0, w, h);
		//ctx.drawImage(points_img, 200, 300, 800, 300);
		break;
    case etat.JEU_EN_COURS :
		updateGame();
		break;
    case etat.GAME_OVER :
		
		gameover_page = new Image();
		gameover_page.src = "/images/gameover.gif";
		ctx.drawImage(gameover_page, 200, 100, 600, 300);
	    clearGame();
		ctx.fillStyle = "white";
		ctx.font = "30px Arial";
		//ctx.fillText("GAME OVER", 400, 200);
		//ctx.fillText("Tapez SPACE", 400, 500);
		break;
	case etat.ATTENTE_JOUEURS :
		start_page = new Image();
		start_page.src = "/images/start_page.png";
		ctx.drawImage(start_page, 0, 0, w, h);
		ctx.fillStyle="#000000";
		ctx.fillRect(220, 490, 600, 50);
		
		break;
	case etat.WAIT :
		
		if(compteurDemarage==0){
			
			etatJeu = etat.JEU_EN_COURS;
			newLevel();
			socket.emit('updateusers');
			
		}
			
		ctx.fillStyle = "red";
		ctx.font = "100px Arial";
		ctx.fillText(compteurDemarage, w/2, h/2);
			
		comptWait++;	
			
		if(comptWait%120 == 0){
			compteurDemarage--;
		}	
		
		
		
		break;
  }
	
	requestAnimationFrame(mainloop);
}

function clearGame(){
	VaisseauJoueur.vie = 3;
	VaisseauJoueur.setVivante(true);
	VaisseauJoueur.score = 0;
	
	missiles = new Array();
	creationBoucliers();
	creationEnemis();
}

function newLevel() {
	
	missiles = new Array();
	creationEnemis();
	creationBoucliers();
}

function nextLevel() {
	niveau++;
	newLevel();
}

function Vivant(){

	if(VaisseauJoueur.getVivante()){
		
		for( name in OtherPlayersVaisseau){
			if(!OtherPlayersVaisseau[name].getVivante()){
				return false;
			}
		}
		
		return true;
		
	}
	return false;

}

function updateGame() {

	dessinEntete();
	
	if(Vivant()){
		collisionCadreJoueur();
	}
	
	if(Vivant()){
		dessinMissile();
	}else{
		effacerMissile();
	}
	
	
	dessinVaisseauJoueur();

	if(Vivant()){
		collisionCadreAlien();
	}
	
	if(Vivant()){
		gestionVaisseauRouge();
	}
	dessinAlien();
	
	dessinBoucliers();
	
	collisionBoucliers();
	
	if(Vivant()){
		tirAlien();
	}
	if(Vivant()){
		compt++;
	}
	
	//pour passer au niveau suivant
	var nbEnemis = 0;
	for(var i = 0; i < VaisseauEnemis.length; i++){
		for(var j = 0 ; j <VaisseauEnemis[i].length;j++){
			if(VaisseauEnemis[i][j].getVivante()){
				nbEnemis++;
			}
		}
	}
		
	//game over
	
	if (VaisseauJoueur.getVie() <= 0){
		etatJeu = etat.GAME_OVER;
		socket.emit("GAME_OVER");
	}
		
	for(var i = 0; i < VaisseauEnemis.length; i++){
		for(var j = 0 ; j <VaisseauEnemis[i].length;j++){
			if(VaisseauEnemis[i][j].getVivante() && VaisseauEnemis[i][j].getY() + VaisseauEnemis[i][j].height > VaisseauJoueur.getY()){
				etatJeu = etat.GAME_OVER;
				socket.emit("GAME_OVER");
			}
		}
	}
	recupImgBouclier();
	
		if (nbEnemis == 0) {
		nextLevel();
	}
	
	//requestAnimationFrame(anime);
}

function gestionVaisseauRouge() {
	if(compt % 300 == 0){
		VaisseauRouge.setSpeedX(-5);
	}
	if(VaisseauRouge.getVivante() && VaisseauRouge.getX() + 70> 0){
		VaisseauRouge.move();
		VaisseauRouge.effacer();
	}else{
		VaisseauRouge.setSpeedX(0);
		VaisseauRouge.setX(w+70);
		VaisseauRouge.setVivante(true);
	}
}

function dessinEntete(){
	/*
	ctx.fillStyle = "white";
	ctx.font = "30px Arial";
	ctx.fillText("Vie " + VaisseauJoueur.vie, 900, 50);
	*/
	// score
	ctx.drawImage(spritesheet2, 
			358, 1417, // debut de ou commencer le crop
			130, 30, // taille image
			50, 0, // coordonné sur le dessin
			130, 30); // taille image
			
	translate_numero(VaisseauJoueur.getScore(),200,0);
	
	// niveau
	ctx.drawImage(spritesheet2, 
		358, 1453, // debut de ou commencer le crop
		130, 30, // taille image
		350, 0, // coordonné sur le dessin
		130, 30); // taille image
			
	ctx.fillStyle = "white";
	ctx.font = "30px Arial";
	//ctx.fillText(niveau, 500,25);
	//var ch = chiffres[niveau];
	//ctx.drawImage(spritesheet2,ch.x, ch.y, ch.width, ch.height, 500, 0, 20, 30);

	translate_numero(niveau,500,0);
	// vie
	ctx.drawImage(spritesheet2, 
			505, 1417, // debut de ou commencer le crop
			110, 30, // taille image
			650, 0, // coordonné sur le dessin
			110, 30); // taille image
		
	var i = VaisseauJoueur.getVie();
	while (i > 0) { 
		ctx.drawImage(spritesheet2, 
			350, 1160, // debut de ou commencer le crop
			110, 50, // taille image
			700 + i * 60 , 0, // coordonné sur le dessin
			50, 25); // taille image
		i -=1;
	}
}

function translate_numero(nb, xc, yc){
	var ch, output = [], sx = nb.toString();

	for (var i = 0, len = sx.length; i < len; i += 1) {
    output.push(+sx.charAt(i));
	}
	for(var i = 0; i < output.length; i++) {
		ch = chiffres[output[i]];
		ctx.drawImage(spritesheet2,ch.x, ch.y, ch.width, ch.height, xc+ i*25, yc, 20, 30);
	}
		
	
}
function dessinMissile(){
	nbMis = 0;
	for(mis in missiles){
		nbMis++
		if(!missiles[mis].draw(ctx)){
			missiles[mis] = undefined;
			delete missiles[mis];
		}	
	}
}

function effacerMissile(){
	for(mis in missiles){
		missiles[mis].effacer();
		missiles[mis] = undefined;
		delete missiles[mis];
	}
}

function moveMissile(id,y){
	
	if(id in missiles){
		missiles[id].setY(y);
	}
}

function tirAlien(){
	
	if(compt%100 == 0){
	
		// donne un nombre rd de tir
		nb = (Math.random() * 4 | 0); 
	
		for (k = 0 ; k < nb ; k++){
			
			num = (Math.random() * VaisseauEnemis.length | 0); 
			
			for(i = VaisseauEnemis[0].length-1 ; i >= 0 ; i-- ){
				if(VaisseauEnemis[num][i].getVivante()){
					
					VaisseauEnemis[num][i].tir(5);
					
					break;
				}
			}
		}
	}
	
}

function collisionCadreJoueur(){
	
	if(VaisseauJoueur.testCollision()){
	
		VaisseauJoueur.effacer();
		VaisseauJoueur.move();
	}
	
	for( name in OtherPlayersVaisseau ){
	
		if(OtherPlayersVaisseau[name].testCollision()){
			OtherPlayersVaisseau[name].effacer();
			OtherPlayersVaisseau[name].move();
		}
	}
	
}

function collisionCadreAlien(){

	// detection collision sur les bords du cadre
	if(!(VaisseauEnemis[0][0].testCollision() 
		&& VaisseauEnemis2[0][0].testCollision())
		|| !(VaisseauEnemis[VaisseauEnemis.length-1][0].testCollision() 
		&& VaisseauEnemis2[VaisseauEnemis.length-1][0].testCollision())){

		for(i = 0; i < VaisseauEnemis.length ; i++){ 		
			for(k = 0; k < VaisseauEnemis[i].length ; k++){ 	
			
				VaisseauEnemis[i][k].setSpeedY(20);
				VaisseauEnemis2[i][k].setSpeedY(20);
				
				VaisseauEnemis[i][k].setSpeedX(-VaisseauEnemis[i][k].getSpeedX());
				VaisseauEnemis2[i][k].setSpeedX(-VaisseauEnemis2[i][k].getSpeedX());
				
			}
		}		
	}
	
	for(i = 0; i < VaisseauEnemis.length ; i++){ 
		for(k = 0; k < VaisseauEnemis[i].length ; k++){ 	
			if(compt%vitesseDeplacementEnnemi == 0){			
				VaisseauEnemis[i][k].effacer();
				VaisseauEnemis[i][k].move();
				VaisseauEnemis2[i][k].effacer();
				VaisseauEnemis2[i][k].move();
				VaisseauEnemis[i][k].setSpeedY(0);
				VaisseauEnemis2[i][k].setSpeedY(0);
				
			}
		}
	}
	
}

function recupImgBouclier() {
	var img;
	for(var i = 0; i <Boucliers.length;i++){
		img = ctx.getImageData(Boucliers[i].x,Boucliers[i].y,Boucliers[i].width,Boucliers[i].height);
		Boucliers[i].setImgData(img);
	}
	
}

function collisionBoucliers(){
	for(var i = 0; i <Boucliers.length;i++){
		Boucliers[i].collision();
	}
}

function dessinBoucliers(){
	ctx.save();
	for(var i = 0; i <Boucliers.length;i++){
		Boucliers[i].draw(ctx);
	}
	ctx.restore();
}

function dessinVaisseauJoueur(){
	
	ctx.save();
	for( name in OtherPlayersVaisseau ){
		OtherPlayersVaisseau[name].draw(ctx);
	}
	VaisseauJoueur.draw(ctx);
	ctx.restore();
	
}

function dessinAlien(){
	ctx.save();
	if(compt%vitesseDeplacementEnnemi == 0){
		if(patron){
			socket.emit("ChangeAlienVisuel",game);
		}
	}
	
	for(i = 0; i < VaisseauEnemis.length ; i++){ 
		for(k = 0; k < VaisseauEnemis[i].length ; k++){ 
		
			if(normal){
				VaisseauEnemis[i][k].draw(ctx);
			}else{
				VaisseauEnemis2[i][k].draw(ctx);
			}
		}
	}
	VaisseauRouge.draw(ctx);
	ctx.restore();
}

function TIRJoueur(name,vitesse,x,y){
	shootSon.play();
	if(name == username){
		VaisseauJoueur.tir(vitesse,x,y);
		
	}else{
		OtherPlayersVaisseau[name].tir(vitesse,x,y);
	}
	
}

function TIRAlienNow(vitesse,x,y,lig,col){
	if(VaisseauEnemis[lig][col].getVivante()){
		shootSon.play();
		VaisseauEnemis[lig][col].tir(vitesse,x,y);
	}
}

function MAJPOSALIEN(x,y,lign,col){
	
	VaisseauEnemis[lign][col].setX(x);
	VaisseauEnemis[lign][col].setY(y);
	VaisseauEnemis2[lign][col].setX(x);
	VaisseauEnemis2[lign][col].setY(y);
	
}

function moveVaisseauRouge(x,y){

	VaisseauRouge.setX(x);
	VaisseauRouge.setY(y);
	
}

function move(evt){
	

	if( evt.which == 39 ){
		VaisseauJoueur.setSpeedX(5);
	}
	if( evt.which == 37 ){
		VaisseauJoueur.setSpeedX(-5);
	}
	if( evt.which == 32 ){
		if(etatJeu === etat.JEU_EN_COURS){
			VaisseauJoueur.tir(-10);
			
		}
	}
}

function arret(evt){
	if(evt.which == 32) { // space
		if(etatJeu === etat.MENU_DEMARRAGE) {
			newLevel();
			etatJeu = etat.JEU_EN_COURS;
		}else if(etatJeu === etat.GAME_OVER) {
			/*niveau = 0;
			nextLevel();
			etatJeu = etat.MENU_DEMARRAGE;*/
			window.location.reload();
		}
	}
	
	if( evt.which == 39 || evt.which == 37 ){
		VaisseauJoueur.setSpeedX(0);
	}
	
}