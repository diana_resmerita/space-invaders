
function Bouclier(xd, yd, tX, tY ,tXC, tYC, Cx , Cy) {

	var x = xd;
	var y = yd;
	var vivante = true;
	var tXCrop = tXC;
	var tYCrop = tYC;
	var cropX = Cx;
	var cropY = Cy;
	var tailleX = tX;
	var tailleY = tY;
	var vie = 10;
	var imgData;
	var damage = 0;
	
	
	var draw = function(ctx) {
		if(vie >=0){
			if(imgData==undefined){
				ctx.drawImage(spritesheet2, 
					cropX, cropY, // debut de ou commencer le crop
					tXCrop, tYCrop, // taille image
					x, y, // coordonné sur le dessin
					tailleX, tailleY); // taiile image
			}else{
				ctx.putImageData(imgData, x, y);
				hit(ctx);
			}
		}
	};
	
	var hit = function(ctx){
	while(damage < 10 - vie){
		for(var i = 0 ;i < 60; i++){
			var randX = Math.random() * (tailleX) ;
			var randY = Math.random() * (tailleY);
			ctx.clearRect(x+randX,y+randY, 6,6);
		}
		damage++
	}
		}
	
	var collisionMissile = function(){
		for(mis in missiles){ // collision PROBLEME missile Enemis 
			if((x + tailleX) > (missiles[mis].x) && x < (missiles[mis].x + missiles[mis].width)){
				if(y  <= (missiles[mis].getY() + missiles[mis].height) && (y + tailleY  ) >= (missiles[mis].getY())){
					vie--;
					missiles[mis].effacer();
					delete missiles[mis];
					return true;
				}
			}
		}
		return false;
	}
	
	var collisionAlien = function(){
		var e;
		for(var i = 0; i < VaisseauEnemis.length; i++){
			for(var j = 0 ; j <VaisseauEnemis[i].length;j++){
				e = VaisseauEnemis[i][j];
				if(e.getVivante() && 
				x < e.getX() + e.width && x + tailleX > e.getX() && y < e.getY() + e.height){
					this.vie-- ;
					return true;
				}
			}
		}
		return false;
	}
	
	
	var collision = function(){
		if(vie>=0){
			return collisionMissile() || collisionAlien();
		}
	}
	
	var setImgData = function(img){
		imgData = img;
	}
	
	var getImageData = function(){
		return imgData;
	}
	
	var getVie = function(){
		return vie;
	}
	
	var setVie = function(value){
		vie = value;
	}
		
	var getDamage = function(){
		return damage;
	}
		
	var setDamage = function(value){
		damage = value;
	}

	return{
		x:x,
		y:y,
		width:tailleX,
		height:tailleY,
		setDamage:setDamage,
		getDamage:getDamage,
		getImageData:getImageData,
		getVie:getVie,
		setVie:setVie,
		draw:draw,
		collision:collision,
		setImgData:setImgData
	}
}