
function Vaisseau(xd, yd, tX, tY ,tXC, tYC, Cx , Cy , Cr , Cg , Cb , identifiant, sc ) {

  var id = identifiant;
  var x = xd;
  var y = yd;
  var speedX = 0;
  var speedY = 0;
  var vivante = true;
  var timerRespown = 120;
  var tXCrop = tXC;
  var tYCrop = tYC;
  var cropX = Cx;
  var cropY = Cy;
  var tailleX = tX;
  var tailleY = tY;
  var r = Cr;
  var g = Cg;
  var b = Cb;
  var vie = 3;
  var score = typeof sc !== 'undefined' ?  sc : 0;
  
  var tir = function(speed){
	// creation d un missile
	if(arguments.length == 1){
		if(id.indexOf("joueur_") > -1){
			socket.emit('updatepos',username ,x ,y ,game);
			socket.emit("tirJoueur",username , speed , game,x+(tailleX/2)-10/2,y -10);
			//tir(speed,x+(tailleX/2)-10/2,y - 50);
		}else{
		//	tir(speed,x+(tailleX/2)-10/2,y + tailleY +10);
			lig = id.split(" ")[0];
			col = id.split(" ")[1];
			socket.emit("tirAlien", speed , game,x+(tailleX/2)-10/2,y + tailleY +10,lig,col);
		}
	}else{
		xm = arguments[1];
		ym = arguments[2];
		if(!(id in missiles)){
			missile = "";
			if(id.indexOf("joueur_") > -1){
				missile = new Missile(xm,ym,10,50,9,17,170,607,0,0,0,id);
			}else{
				missile = new Missile(xm,ym,10,50,9,17,170,607,0,0,0,id);
			}
			missile.setSpeedY(speed);
			missiles[id] = missile;
		}
	}
  }
  
  var getSpeedX = function(value){
	return speedX;
  }
  
  var getX = function(value){
	return x;
  }
  
  var getY = function(value){
	return y;
  }
  var setSpeedX = function(value){
	speedX = value;
  }
 
  var setX = function(value){  
	x = value;
  }
  
  var setY = function(value){
  
	y = value;
  }
 
  var setSpeedY = function(value){
	speedY = value;
  }
  
  var effacer = function(){
	ctx.clearRect(x, y,tailleX,tailleY);  
  };
  
  var draw = function(ctx) {
	
	if(vivante){	
		collisionMissile();
	}

	if(!vivante && id.indexOf("joueur_") > -1){
		cropX = 240;
		cropY = 634;
		tXCrop = 107;
		tYCrop = 61;
		if(timerRespown <= 0){
			timerRespown = 120;
			vivante = true;
			cropX = 145;
			cropY = 630;
			tXCrop = 86;
			tYCrop = 63;
			// envoyé respown
			
		}else{
			timerRespown--;
		}
	}else{
		if(id.indexOf("joueur_") > -1){
			cropX = 145;
			cropY = 630;
			tXCrop = 86;
			tYCrop = 63;
		}
	}
	
	if(vivante){
		drawVaisseau(ctx);
	}else if(id.indexOf("joueur_") > -1){
		drawVaisseau(ctx);
	}
		/*if(id in missiles){
			missile = missiles[id];
			if(!missile.draw(ctx)){
				missile = undefined;
				delete missiles[id];
			}
			//console.log(missile);
		}*/
	
	
  };
  
  var drawVaisseau = function(ctx){
	  
	// positionner bas de la fenetre au millieu
	// vaisseau du joueur
	
	ctx.drawImage(spritesheet, 
				cropX, cropY, // debut de ou commencer le crop
				tXCrop, tYCrop, // taille image
				x, y, // coordonné sur le dessin
				tailleX, tailleY); // taiile image
	
	imageData = ctx.getImageData(x, y, tailleX, tailleY);
	var data = imageData.data;

	for(var i = 0; i < data.length; i += 4) {
		// red
		data[i] =  data[i] - r;
		// green
		data[i + 1] =  data[i + 1] - g ;
		// blue
		data[i + 2] =   data[i + 2] - b;
	}		
	
	ctx.putImageData(imageData, x, y);
  };
  
  var collisionMissile = function(){
	
	for(mis in missiles){
		
		if((x + tailleX) > (missiles[mis].x)
		&& (x) < (missiles[mis].x + missiles[mis].width)){

			if((y ) <= (missiles[mis].getY()) 
				&& (y + tailleY  ) >= (missiles[mis].getY())){
				
				identifiantMiss = mis;
				
				missiles[mis].effacer();
				delete missiles[mis];
				
				if(id.indexOf("joueur_") == -1){
				
					if(id =="r") {
						VaisseauRouge.effacer();
						VaisseauRouge.setVivante(false);
						VaisseauJoueur.score += score[Math.floor(Math.random() * score.length)];
					
					}else{
						lig = id.split(" ")[0];
						col = id.split(" ")[1];
						
						VaisseauEnemis2[lig][col].effacer();
						VaisseauEnemis[lig][col].effacer();
						VaisseauEnemis2[lig][col].setVivante(false);
						VaisseauEnemis[lig][col].setVivante(false);
						VaisseauEnemis[lig][col].destruction();
						if(identifiantMiss.replace("joueur_","") in OtherPlayersVaisseau){
							OtherPlayersVaisseau[identifiantMiss.replace("joueur_","")].setScore( OtherPlayersVaisseau[identifiantMiss.replace("joueur_","")].getScore() + VaisseauEnemis[lig][col].getScore());
						}else{
							VaisseauJoueur.setScore( VaisseauJoueur.getScore() + VaisseauEnemis[lig][col].getScore());
						}
					}
				}else{
					// envoyé mort
					
					vie -=1; // vie
					effacer();
					destruction();
				}
				
				// joue son aussi	
				boomSon.load();
				boomSon.play();
			}
		}
	}
	
  };
  
  var testCollision = function(){
	  
	value = x + speedX ;

	if((value+tailleX) >= w){
		return false;
	}
	
	if(value <= 0){
		return false;
	}
	
	return true;  
	  
  }
  
  var move = function() {
	//if(vivante){
		/*if( id.indexOf("joueur_") > -1 ){
			x = x + speedX;
			y = y + speedY;
		}*/
		if(speedX != 0 || speedY != 0){
		
			if(id.indexOf("joueur_") > -1){
			
				socket.emit('updatepos',username ,x + speedX ,y + speedY, game);
				
			}else if (id =="r"){
			
				/* 
					x = x + speedX;
					y = y + speedY;	
				*/
				
				socket.emit('moveVaisseauRouge', x + speedX , y + speedY );
				
			}else{
				
				lig = id.split(" ")[0];
				col = id.split(" ")[1];		
				if(patron){
					
					socket.emit('moveAlien', x + speedX , y + speedY ,lig ,col,game , compt);
					
				}
				
			}
		}
		
	//}
  };
  
  var setMissile = function(value){
	 
	missile = value;
	  
  }
  
  var setVivante = function(value){
	  vivante = value;
  }
  
  var getVivante = function(){
	return vivante;  
  }
  
  var getVie= function(){
	return vie;
  }
  
  var getScore = function(){
	return score;
  }
  
  var setScore = function(value){
	score = value;
  }
  
  var destruction = function(){
	if(id.indexOf("joueur_") == -1){
		cropX = 358;
		cropY = 631;
		tXCrop = 97;
		tYCrop = 60;
	}
	vivante = false;
	drawVaisseau(ctx);
	
  }
  
  return{
	x:x,
	y:y,
	getX:getX,
	getY:getY,
	setX:setX,
	setY:setY,
	width:tailleX,
	height:tailleY,
	speedX:speedX,
	setSpeedX:setSpeedX,
	setSpeedY:setSpeedY,
	setMissile:setMissile,
	speedY:speedY,
	destruction:destruction,
	getSpeedX:getSpeedX,
	setVivante:setVivante,
	getVivante:getVivante,
	move:move,
	testCollision:testCollision,
	draw:draw,
	tir:tir,
	effacer:effacer,
	getVie:getVie,
	getScore:getScore,
	setScore:setScore
  };
}
