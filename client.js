var username = "essaie";// prompt("What's your name?");
var conversation, data, datasend, users,newPlayer = false;;
var listOfplayers = Array();
var ListGames = Array();
var socket = io.connect();
var game = "Accueil";
var patron = false;

//-----------------------------------------------------------------------------
//								connect
//-----------------------------------------------------------------------------
socket.on('connect', function(){

	socket.emit('coclient', username);

	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								valide		
//-----------------------------------------------------------------------------
socket.on('valide', function() {
	GotoGameTwo();
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								invalide		
//-----------------------------------------------------------------------------
socket.on('invalide', function() {
	alert("Pseudo invalide. null ou deja utilisé.");
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								updateName
//-----------------------------------------------------------------------------
socket.on('updateName', function(name){

	username = name;
	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								GAME_OVER
//-----------------------------------------------------------------------------
socket.on('GAME_OVER', function(name){

	delete OtherPlayersVaisseau[name];
	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								updateListGames
//-----------------------------------------------------------------------------
socket.on('updateListGames', function(list, listPlayer){
	console.log("updateListGames");
	ListGames = list;
	games.innerHTML = "";

	html = "<table>";
		html += "<tr>";
			html += "<td>";
				html += "Name";
			html += "</td>";
			html += "<td>";
				html += "Nb/Max";
			html += "</td>";
		html += "</tr>";
	for( game in ListGames ){
		nbGame_over = 0;
		for( name in listPlayer[game]){
			if(listPlayer[game][name].game_over){
				nbGame_over++;
			}
		}
		
		if( nbGame_over != Object.keys(ListGames[game]).length - 1 ){
		
			if(game != 'Accueil'){
				MaxPlayers = ListGames[game][0];
				NbPlayers = Object.keys(ListGames[game]).length - 1;
				if(MaxPlayers == NbPlayers){
					enable="disabled";
				}else{
					enable = "";
				}
				html += "<tr>";
				var gameLineOfHTML = '<td>' + game + '&nbsp;&nbsp;&nbsp;' + '</td><td>'+ NbPlayers +'/' + MaxPlayers + '</td><td><input type="button" value="rejoindre" onclick="GotoGame(\''+game+'\');" '+enable+' > </input> ' + '</td>';
				html += gameLineOfHTML;
				html += "</tr>";
			}
		}
	}
	html += "<table>";
	games.innerHTML = html;
	
	if(game != "Accueil"){
		if(etatJeu === etat.ATTENTE_JOUEURS ){
			
			MaxPlayers = ListGames[game][0];
			NbPlayers = Object.keys(ListGames[game]).length - 1;
			
			if(MaxPlayers == NbPlayers){
				document.querySelector("#ALERT").innerHTML = "";
			}else{
				etatJeu = etat.ATTENTE_JOUEURS ;
				document.querySelector("#ALERT").innerHTML = "En attente de "+ (MaxPlayers - NbPlayers) +" joueur(s)";
			}
		}
	}
	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								updatechat		
//-----------------------------------------------------------------------------
socket.on('updatechat', function (username, data) {
	/*var chatMessage = "<b>" + username + " : </b> " + data + "<br>";
	conversation.innerHTML += chatMessage; */
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								moveMissile		
//-----------------------------------------------------------------------------
socket.on('moveMissile', function (id,y) {
	//console.log('moveMissile  ');

	moveMissile(id,y);
	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								moveAlien		
//-----------------------------------------------------------------------------
socket.on('moveAlien', function (x,y,lign,col,comp) {
	//console.log('moveAlien  ');

	MAJPOSALIEN(x,y,lign,col);
	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								moveVaisseauRouge		
//-----------------------------------------------------------------------------
socket.on('moveVaisseauRouge', function (x,y) {
	//console.log('moveVaisseauRouge  ');

	moveVaisseauRouge(x,y);
	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								updtaeCompt		
//-----------------------------------------------------------------------------
socket.on('updateCompt', function (comp) {
//	console.log('updtaeCompt  ' + comp);
	compt = comp;
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								updatepos		
//-----------------------------------------------------------------------------
socket.on('updatepos', function (username,x,y) {
	console.log('updatepos');
	updatePlayerNewPos(username,x,y);
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//								updateusers		
//-----------------------------------------------------------------------------
socket.on('updateusers', function(listOfUsers) {

	console.log("updateusers");
	//users.innerHTML = "";
	for(var name in listOfUsers) {
		if(name != username){
			//var userLineOfHTML = '<div>' + name + '</div>';
			//users.innerHTML += userLineOfHTML;
			creationVaisseauJoueur(name);
		}
	}
	
	if(newPlayer){
	
		newPlayer = false;
	
		socket.emit('updatepos',username ,VaisseauJoueur.getX() ,VaisseauJoueur.getY() ,game);
		tab = {};
		tab2 = {};
		for(i = 0 ; i < Boucliers.length ; i++){
			tab[i] = {};
			tab[i]["vie"] = Boucliers[i].getVie();
			tab[i]["damage"] = Boucliers[i].getDamage();
		}
		for(i = 0 ; i < VaisseauEnemis.length ; i++){
			tab2[i] = {};
			for(j = 0 ; j < VaisseauEnemis[i].length ; j++){
				tab2[i][j] = VaisseauEnemis[i][j].getVivante();
			}
		}
		console.log("SEND BOUBOU !! !! ! !!");
		console.log(tab);
		socket.emit('updatebouclier',tab);
		socket.emit('updateAlien',tab2);
	}
	
	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								updateAlien		
//-----------------------------------------------------------------------------
socket.on('updateAlien', function(tabInfoAlien) {

	console.log("updateAlien");
	console.log(tabInfoAlien);

	for(index in tabInfoAlien) {
		for(index2 in tabInfoAlien[index]) {
	
			VaisseauEnemis[index][index2].setVivante(tabInfoAlien[index][index2]);
			VaisseauEnemis2[index][index2].setVivante(tabInfoAlien[index][index2]);
		}
	}
	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								updatebouclier		
//-----------------------------------------------------------------------------
socket.on('updatebouclier', function(tabInfoBoubou) {

	console.log("updatebouclier");
	console.log(tabInfoBoubou);

	for(index in tabInfoBoubou) {
	
		Boucliers[index].setVie(tabInfoBoubou[index]["vie"]);
		Boucliers[index].setDamage(tabInfoBoubou[index]["damage"]);
		
	}
	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								updatePlayers		
//-----------------------------------------------------------------------------
socket.on('updatePlayers', function(list) {
	console.log("updatePlayers");
	listOfplayers = list;
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								updatelevel		
//-----------------------------------------------------------------------------
socket.on('updatelevel', function(level) {
	console.log("updatelevel");
	niveau = level;
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								decoPlayer		
//-----------------------------------------------------------------------------
socket.on('decoPlayer', function(name) {
	OtherPlayersVaisseau[name] = undefined;
	delete OtherPlayersVaisseau[name];
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								tirJoueur		
//-----------------------------------------------------------------------------
socket.on('tirJoueur', function(name,vitesse,x,y) {
	//console.log("tirJoueur");
	TIRJoueur(name,vitesse,x,y);
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								tirAlien		
//-----------------------------------------------------------------------------
socket.on('tirAlien', function(vitesse,x,y,lig,col) {
	//console.log("tirAlien");
	TIRAlienNow(vitesse,x,y,lig,col);
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								ChangeAlienVisuel		
//-----------------------------------------------------------------------------
socket.on('ChangeAlienVisuel', function() {
	//console.log("ChangeAlienVisuel");
	normal = !normal
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								Lancement		
//-----------------------------------------------------------------------------
socket.on('Lancement', function() {
	console.log("Lancement");
	if(etatJeu != etat.JEU_EN_COURS){
		etatJeu = etat.WAIT;
		socket.emit('demarrage', username , game);
	}else{
		console.log("NEW PLAYER");
		newPlayer = true;
		socket.emit('updatelevel', niveau);
	}
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//								ChoosePatron		
//-----------------------------------------------------------------------------
socket.on('ChoosePatron', function() {
	console.log("ChoosePatron");
	patron = true;
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//								MiseEnplaceGame		
//-----------------------------------------------------------------------------
socket.on('MiseEnplaceGame', function(nameGame ) {
	console.log("MiseEnplaceGame");
	
	game = nameGame;
	
	document.querySelector("#jeu").style.display = 'block';
	document.querySelector("#MESS").innerHTML = "Bienvenue " + username ;
	document.querySelector("#MESS").innerHTML += "&nbsp;&nbsp;|&nbsp;&nbsp;Name Game : " + game ;
	document.querySelector("#ENTETE").style.display = 'block';
	document.querySelector("#Accueil").style.display = 'none';
	
	init();

	creationJoueur();
	
	MaxPlayers = ListGames[nameGame][0];
	NbPlayers = Object.keys(ListGames[nameGame]).length - 1;
	
	if(MaxPlayers > NbPlayers){
		etatJeu = etat.ATTENTE_JOUEURS ;
		document.querySelector("#ALERT").innerHTML = "En attente de "+ (MaxPlayers - NbPlayers) +" joueur(s)";
	}
	
});
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


function deco(){
	socket.emit('deco', username);
	socket.disconnect();
}

window.onbeforeunload = function(e) {
	deco();		
};

// on load of page
window.addEventListener("load", function(){
	// get handles on various GUI components
	/*conversation = document.querySelector("#conversation");
	data = document.querySelector("#data");
	datasend = document.querySelector("#datasend");
	users = document.querySelector("#users");

	// Listener for send button
	datasend.addEventListener("click", function(evt) {
		sendMessage();
	});

	// detect if enter key pressed in the input field
	data.addEventListener("keypress", function(evt) {
		// if pressed ENTER, then send
		if(evt.keyCode == 13) {
			this.blur();
			sendMessage();
			this.focus();
		}
	});

	// sends the chat message to the server
	function sendMessage() {
		var message = data.value;
		data.value = "";
		// tell server to execute 'sendchat' and send along one parameter
		socket.emit('sendchat', message);
	}		*/
});
